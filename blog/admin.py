from django.contrib import admin
from blog.models import Post, TitledPost

# changes the order of the information displayed, adds markup
class PostAdmin(admin.ModelAdmin):
	prepopuated_fields = {"slug":("title",)}
	list_display = ('text', 'pub_date','was_published_recently')
	fieldsets = [
		(None,               {'fields': ['text']}),
		('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
	]
	list_filter = ['pub_date']
	search_fields = ['text']
	date_hierarchy = 'pub_date'

# changes the order of the information displayed, adds markup
class TitledPostAdmin(admin.ModelAdmin):
	list_display = ('title', 'pub_date','was_published_recently')
	prepopuated_fields = {"slug":("title")}
	fieldsets = [
		(None,               {'fields': ['title']}),
		(None,               {'fields': ['text']}),
		(None, 				 {'fields': ['slug']}),
		('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
	]
	list_filter = ['pub_date']
	search_fields = ['text']
	date_hierarchy = 'pub_date'

admin.site.register(Post, PostAdmin)
admin.site.register(TitledPost, TitledPostAdmin)