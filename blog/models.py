from django.db import models
from django.utils import timezone
import datetime

class Post(models.Model):
	text = models.CharField(max_length=1000)
	pub_date = models.DateTimeField('date published')
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'
	def __unicode__(self):  # Python 3: def __str__(self):
		return self.text

class TitledPost(models.Model):
	title = models.CharField(max_length=200)
	text = models.TextField()
	pub_date = models.DateTimeField('date published')
	slug = models.SlugField(max_length=40, unique=True)
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'
	def __unicode__(self):  # Python 3: def __str__(self):
		return self.title
