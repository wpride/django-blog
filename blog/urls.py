from django.conf.urls import patterns, url

from blog import views

urlpatterns = patterns('',
	url(r'^$', views.IndexView.as_view(), name='index'),
	url(r'^titledposts$', 'blog.views.getRecentPosts'),
	url(r'^titledposts/$', 'blog.views.getRecentPosts'),
	url(r'^titledposts/(?P<selected_page>\d+)/?$', 'blog.views.getRecentPosts'),
	url(r'^titledposts/(?P<postSlug>[-a-zA-Z0-9]+)/?$', 'blog.views.getPost'),
	# Blog posts
)