from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from blog.models import Post, TitledPost
from django.core.paginator import Paginator, EmptyPage

class IndexView(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'latest_post_list'

    def get_queryset(self):
        """Return the last five published polls."""
        return Post.objects.order_by('-pub_date')[:5]

def getRecentPosts(request, selected_page=1):
	# Get all blog posts
	posts = TitledPost.objects.all().order_by('-pub_date')

	# Sort posts into chronological order
	posts = Paginator(posts, 5)
	
	try:
		returned_page = posts.page(selected_page)
	except EmptyPage:
		returned_page = posts.page(posts.num_pages)

	# Display all the posts
	return render_to_response('titledposts.html', { 'posts':returned_page.object_list,'page':returned_page})
	
def getPost(request, postSlug):
    # Get specified post
    post = TitledPost.objects.filter(slug=postSlug)

    # Display specified post
    return render_to_response('posts.html', { 'posts':post})