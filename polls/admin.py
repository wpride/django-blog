from django.contrib import admin
from polls.models import Choice, Poll


class ChoiceInline(admin.TabularInline):
	# class reference
    model = Choice
	# how many slots
    extra = 3

# changes the order of the information displayed, adds markup
class PollAdmin(admin.ModelAdmin):
	list_display = ('question', 'pub_date','was_published_recently')
	fieldsets = [
		(None,               {'fields': ['question']}),
		('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
	]
	inlines = [ChoiceInline]
	list_filter = ['pub_date']
	search_fields = ['question']
	date_hierarchy = 'pub_date'

admin.site.register(Poll, PollAdmin)